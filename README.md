## Pinebook Pro Debian Post-install Script

This script is intended to be run to perform some tweaks after installing Debian
on the Pinebook Pro via
[this installer](https://github.com/daniel-thompson/pinebook-pro-debian-installer/).

Right now, it:

* Downloads and installs Bluetooth firmware
* Installs a keymapping for the brightness keys
* More in the future?
