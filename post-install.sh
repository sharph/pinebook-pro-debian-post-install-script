#!/bin/bash

set -e

sudo apt-get install -y wget

## bluetooth

sudo wget -O /usr/lib/firmware/BCM4345C5.hcd \
    https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware/raw/master/BCM4345C5.hcd?inline=false
sudo cp /usr/lib/firmware/BCM4345C5.hcd /usr/lib/firmware/brcm/BCM.hcd
sudo cp /usr/lib/firmware/BCM4345C5.hcd /usr/lib/firmware/brcm/

#sudo wget -O /usr/lib/firmware/nvram_ap6256.txt \
#    https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware/raw/master/nvram_ap6256.txt?inline=false
#sudo cp /usr/lib/firmware/nvram_ap6256.txt /usr/lib/firmware/bcrm/brcmfmac43456-sdio.pine64,pinebook-pro.txt
#
#sudo wget -O /usr/lib/firware/brcm/brcmfmac43456-sdio.bin \
#    https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware/raw/master/fw_bcm43456c5_ag.bin?inline=false
#
#sudo wget -O /usr/lib/firmware/brcm/brcmfmac43456-sdio.clm_blob \
#    https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware/raw/master/brcmfmac43456-sdio.clm_blob?inline=false

## brightness buttons

sleep 1

sudo bash -c "cat > /etc/udev/hwdb.d/10-usb-kbd.hwdb" <<EOF
evdev:input:b0003v258Ap001E*
  KEYBOARD_KEY_700a5=brightnessdown
  KEYBOARD_KEY_700a6=brightnessup
  KEYBOARD_KEY_70066=sleep
EOF

sudo udevadm hwdb --update

echo "Reboot to apply changes!"
